import { googleDriveAccess } from "https://bitbucket.org/sagal/gdrive_js/src/master/google_drive_access.js";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const googleClientId = Deno.env.get("GOOGLE_CLIENT_ID");
const googleSecret = Deno.env.get("GOOGLE_SECRET");
const refreshToken = Deno.env.get("GOOGLE_REFRESH_TOKEN");
const articleFilenames = Deno.env.get("ARTICLE_FILENAMES").split(",");
const articleRootDirectories = Deno.env
  .get("ARTICLE_ROOT_DIRECTORIES")
  .split(",");
const stockFilenames = Deno.env.get("STOCK_FILENAMES").split(",");
const stockRootDirectories = Deno.env.get("STOCK_ROOT_DIRECTORIES").split(",");

let articleMap = {};
let categoryMap = {};

let accessToken = await getAccessToken();
await processAllArticleFiles(accessToken);
await processAllStockFiles(accessToken);
for (let articleData of Object.values(articleMap)) {
  await sagalDispatch(articleData);
}

async function processAllArticleFiles(accessToken) {
  let articleFilesWithDirectory = [];
  if (articleFilenames.length == articleRootDirectories.length) {
    for (let i = 0; i < articleFilenames.length; i++) {
      articleFilesWithDirectory.push({
        filename: articleFilenames[i],
        folder: articleRootDirectories[i],
      });
    }
  }
  for (let articleFileAndFolder of articleFilesWithDirectory) {
    let articleDocument =
      await googleDriveAccess.getSpecificFileFromFolderByName(
        articleFileAndFolder.filename,
        articleFileAndFolder.folder,
        accessToken,
        {}
      );
    articleDocument = await articleDocument.text();
    await processArticleDocument(articleDocument);
  }
}

async function processAllStockFiles(accessToken) {
  let stockFilesWithDirectory = [];
  if (stockFilenames.length == stockRootDirectories.length) {
    for (let i = 0; i < stockFilenames.length; i++) {
      stockFilesWithDirectory.push({
        filename: stockFilenames[i],
        folder: stockRootDirectories[i],
      });
    }
  }
  for (let stockFileAndFolder of stockFilesWithDirectory) {
    let stockDocument = await googleDriveAccess.getSpecificFileFromFolderByName(
      stockFileAndFolder.filename,
      stockFileAndFolder.folder,
      accessToken,
      {}
    );
    stockDocument = await stockDocument.text();
    await processStockDocument(stockDocument);
  }
}

async function getAccessToken() {
  try {
    let accessToken = await googleDriveAccess.getGoogleDriveAccessToken(
      googleClientId,
      googleSecret,
      refreshToken
    );
    return accessToken;
  } catch (e) {
    console.log(`Authorization failed: ${e.message}`);
  }
}

async function processArticleDocument(articleDocument) {
  let documentData = articleDocument.split("\n");

  for (let documentLine of documentData) {
    try {
      let docLineCode = documentLine.slice(0, 3);
      let docLineBody = documentLine.substring(3);
      if (docLineCode == "SRU") {
        await processCategory(docLineBody);
      } else if (docLineCode == "ART") {
        await processArticle(docLineBody);
      } else if (docLineCode == "PRE") {
        await processPrice(docLineBody);
      }
    } catch (e) {
      console.log(`Unable to process line: ${e.message}`);
    }
  }
}

async function processCategory(categoryLine) {
  let category = categoryLine.split("|");

  categoryMap[category[0].toString().trim()] = category[1].toString().trim();
}

async function processArticle(articleLine) {
  let article = articleLine.split("|");

  let articleSku = article[0].toString().trim();
  let articleName = article[1].toString().trim();
  let articleDescription = article[1].toString().trim();
  let articleCategoryKey = article[4].toString().trim();

  let articleCategory = categoryMap[articleCategoryKey];

  let processedArticle = {
    sku: articleSku,
    client_id: clientId,
    merge: false,
    integration_id: clientIntegrationId,
    ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
      return {
        ecommerce_id: ecommerce_id,
        properties: [
          { name: articleName },
          { description: articleDescription },
          { categories: { [articleCategoryKey]: articleCategory } },
        ],
        variants: [],
      };
    }),
  };

  articleMap[articleSku] = processedArticle;
}

async function processPrice(priceLine) {
  let price = priceLine.split("|");

  let priceArticleSku = price[0].toString().trim();
  let priceEcommerceNumber = price[1].toString().trim();
  let priceValue = Number(price[3].toString().trim());

  if (articleMap[priceArticleSku]) {
    if (priceEcommerceNumber == "6") {
      let indexOfEcommerceData = articleMap[
        priceArticleSku
      ].ecommerce.findIndex((it) => {
        return it.ecommerce_id == clientEcommerce["MELI"];
      });
      articleMap[priceArticleSku].ecommerce[
        indexOfEcommerceData
      ].properties.push({
        price: {
          value: priceValue,
          currency: "$",
        },
      });
    } else if (priceEcommerceNumber == "1") {
      let indexOfEcommerceData = articleMap[
        priceArticleSku
      ].ecommerce.findIndex((it) => {
        return it.ecommerce_id == clientEcommerce["VDS"];
      });
      articleMap[priceArticleSku].ecommerce[
        indexOfEcommerceData
      ].properties.push({
        price: {
          value: priceValue,
          currency: "$",
        },
      });
    }
  }
}

async function processStockDocument(stockDocument) {
  let documentData = stockDocument.split("\n");

  for (let documentLine of documentData) {
    try {
      let docLineCode = documentLine.slice(0, 3);
      let docLineBody = documentLine.substring(3);
      if (docLineCode == "SKS") {
        await processStock(docLineBody);
      }
    } catch (e) {
      console.log(`Unable to process line: ${e.message}`);
    }
  }
}

async function processStock(stockLine) {
  let stock = stockLine.split("|");

  let stockArticleSku = stock[1].toString().trim();
  let stockAmount = Number(stock[2].toString().trim());

  if (articleMap[stockArticleSku]) {
    articleMap[stockArticleSku].ecommerce.map((ecommerceData) => {
      if (
        ecommerceData.properties.find((property) =>
          Object.keys(property).includes("stock")
        )
      ) {
        let indexOfEcommerceStock = ecommerceData.properties.findIndex(
          (property) => Object.keys(property).includes("stock")
        );
        ecommerceData.properties[indexOfEcommerceStock].stock +=
          stockAmount >= 0 ? stockAmount : 0;
      } else {
        ecommerceData.properties.push({
          stock: stockAmount >= 0 ? stockAmount : 0,
        });
      }
    });
  }
}
