import { googleDriveAccess } from "https://bitbucket.org/sagal/gdrive_js/raw/master/google_drive_access.js";
import { S3Bucket } from "https://deno.land/x/s3@0.4.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const googleClientId = Deno.env.get("GOOGLE_CLIENT_ID");
const googleSecret = Deno.env.get("GOOGLE_SECRET");
const refreshToken = Deno.env.get("GOOGLE_REFRESH_TOKEN");
const imagesDirectory = Deno.env.get("IMAGES_DIRECTORY");
const awsKeyId = Deno.env.get("AWS_ACCESS_KEY_ID");
const awsSecretKey = Deno.env.get("AWS_SECRET_ACCESS_KEY");

const bucket = new S3Bucket({
  accessKeyID: awsKeyId,
  secretKey: awsSecretKey,
  bucket: "sagal-excel-uploads-0181239103940129",
  region: "us-east-1",
});

let imagesMap = {};

let accessToken = await getAccessToken();
await processAllImages(accessToken);
for (let [articleSku, images] of Object.entries(imagesMap)) {
  await sendImagesToSagal(articleSku, images);
}

async function sendImagesToSagal(articleSku, images) {
  let articleImages = images;

  let processedArticle = {
    sku: articleSku,
    client_id: clientId,
    options: {
      merge: false,
    },
    integration_id: clientIntegrationId,
    ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
      let ecommerceProps = {
        ecommerce_id: ecommerce_id,
        properties: [
          {
            images: articleImages,
          },
        ],
        variants: [],
      };
      return ecommerceProps;
    }),
  };

  await sagalDispatch(processedArticle);
}

async function getAccessToken() {
  try {
    let accessToken = await googleDriveAccess.getGoogleDriveAccessToken(
      googleClientId,
      googleSecret,
      refreshToken
    );
    return accessToken;
  } catch (e) {
    console.log(`Authorization failed: ${e.message}`);
  }
}

async function processAllImages(accessToken) {
  let allImageFileInfo = await googleDriveAccess.getAllFilesFromFolder(
    imagesDirectory,
    accessToken,
    {
      extraQueryInfo: `modifiedTime > '2017-06-30T23:00:00'`,
      isPagedRequest: true,
    }
  );
  let buffer = [];
  for (let imageFileInfo of allImageFileInfo) {
    try {
      let batchImageProcessing = async (imageInfoInBuffer) => {
        console.log(`Retrieving image ${imageInfoInBuffer.name}`);

        let imageData = await googleDriveAccess.getSpecificFileContentById(
          imageInfoInBuffer.id,
          accessToken
        );
        imageData = await imageData.blob();
        let imageBuffer = await imageData.arrayBuffer();
        imageData = new Uint8Array(imageBuffer);

        let imageUrl = await uploadImageAndGetURI(
          imageInfoInBuffer.name,
          imageData
        );
        await processImageInArticle(imageInfoInBuffer.name, imageUrl);
      };

      if (buffer.length < 2) {
        buffer.push(imageFileInfo);
      }

      if (buffer.length == 2) {
        await Promise.all(buffer.map(batchImageProcessing));
        buffer = [];
      }
    } catch (e) {
      console.log(
        `Unable to retrieve image ${imageFileInfo.name}: ${e.message}`
      );
    }
  }
  if (buffer.length) {
    try {
      await Promise.all(buffer.map(batchImageProcessing));
      buffer = [];
    } catch (e) {
      console.log(`Unable to upload image batch: ${e.message}`);
    }
  }
}

async function uploadImageAndGetURI(imageName, imageData) {
  try {
    await bucket.putObject(
      `/iberpark_images/${imageName.toLowerCase()}`,
      imageData,
      {
        contentType: "image/jpeg",
        acl: "public-read",
      }
    );
    console.log(
      `Image uploaded: https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/iberpark_images/${imageName.toLowerCase()}`
    );
    return `https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/iberpark_images/${imageName.toLowerCase()}`;
  } catch (e) {
    console.log(`Unable to upload image: ${e.message}`);
  }
}

async function processImageInArticle(imageName, imageUrl) {
  let articleSku = imageName.toLowerCase().split(".")[0].trim();

  if (imagesMap[articleSku]) {
    imagesMap[articleSku].push(imageUrl);
  } else {
    imagesMap[articleSku] = [imageUrl];
  }
}
