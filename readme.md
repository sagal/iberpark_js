# Integration scripts for system: Iberpark

## Introduction

These scripts were created to run in the generic integration platform that links Sagal's clients' various forms of sending data into payloads that the business platform can understand. Iberpark is a client that hosts their article data in Google Drive in the form of text files and images, which must be retrieved to process their articles.

## Workflow

### Article Script (iberpark_articles.js)

- Acquires an identification access token through Drive's API.
- Retrieves all the client's text files with article data from the Drive API using the access token.
- Processes the document line by line, utilizing the start of line's 3 digit code to determine behavior.
- Afterwards, retrieves and processes stock data stored in the client's files through a similar procedure.
- Dispatches the processed articles to Sagal.

### Image Script (iberpark_images.js)

- Acquires an identification access token through Drive's API.
- Retrieves the storage location of the client's images from the Drive API using the access token.
- Processes each image by uploading them to Amazon S3 and linking the URLs to the article they are related to (through their SKU).
- Dispatches the processed images to Sagal.

## Environment Variables

- **CLIENT_ID**: Sagal's known identifier for the client. A number.
- **CLIENT_INTEGRATION_ID**: the integration script's id in the existing runtime environment. A number.
- **CLIENT_ECOMMERCE**: a map holding this client's known identifiers for each E-Commerce service. (e.g: Key MELI for Mercadolibre)
- **GOOGLE_CLIENT_ID**: the client's idetifier for use with the Google Drive API.
- **GOOGLE_SECRET**: the client's secret password needed to retrieve the access token for the Google Drive API.
- **GOOGLE_REFRESH_TOKEN**: the client's refresh token (a permanent token that allows generation of temporary access tokens) for use with the Google Drive API.
- **ARTICLE_FILENAMES**: the filenames of the files containing the client's article data in Google Drive.
- **ARTICLE_ROOT_DIRECTORIES**: the directories where the article files can be found in Google Drive.
- **STOCK_FILENAMES**: the filenames of the files containing the client's stock data in Google Drive.
- **STOCK_ROOT_DIRECTORIES**: the directories where the stock files can be found in Google Drive.
- **IMAGES_DIRECTORY**: the directories where the client's images can be found in Google Drive.
- **AWS_ACCESS_KEY_ID**: the access key ID needed to access Amazon Web Services.
- **AWS_SECRET_ACCESS_KEY**: the secret access key needed to access Amazon Web Services.

## Client Endpoints

- "https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/iberpark_images/{Image_name}": the path to the Amazon S3 database where the client's images are uploaded to for use with Sagal.

## Functions

### Article Script (iberpark_articles.js)

#### getAccessToken()

Returns the Google Drive API access token needed to successfully make requests to the Google Drive API, also acts as an identifier for the client in the Google Drive API.

#### processAllArticleFiles(String accessToken)

Retrieves each file with article data from the client's Google Drive and calls the processArticleDocument function to process the file.

#### processArticleDocument(String articleDocument)

Processes the document line by line, calling three different functions depending on the start-of-line 3 digit code that identifies what type of content the line is describing. This can be SRU for a category, ART for an article, or PRE for pricing data.

#### processCategory(String categoryLine)

Processes the given category data by mapping segments of the string to their respective business value. Segments are split with the pipe ('|') delimiter.

- categoryLine[0]: the category number.
- categoryLine[1]: the name of the category.

#### processArticle(String articleLine)

Processes the given article data by mapping segments of the string to their respective business value. Segments are split with the pipe ('|') delimiter.

- articleLine[0]: the article's SKU.
- articleLine[1]: the article's name/description.
- articleLine[4]: the number of the category that the article belongs to.

#### processPrice(String priceLine)

Processes the given pricing data by mapping segments of the string to their respective business value. Segments are split with the pipe ('|') delimiter.

- priceLine[0]: the article SKU that the pricing data belongs to.
- priceLine[1]: the ecommerce that the listed article pricing is valid for.
- priceLine[3]: the pricing value in Uruguayan pesos.

#### processAllStockFiles(String accessToken)

Retrieves each file with stock data from the client's Google Drive and calls the processStockDocument function to process the file.

#### processStockDocument(String stockDocument)

Processes the document line by line, calling the processStock function if the line's start-of-line 3 digit code corresponds to the value of 'SKS'.

#### processStock(String stockLine)

Processes the given stock data by mapping segments of the string to their respective business value. Segments are split with the pipe ('|') delimiter. A stock property is created if the article has no existing stock, and the stock amount is added to the existing stock property value otherwise.

- stockLine[1]: the article SKU that the stock data belongs to.
- stockLine[2]: the amount of stock available for the article.

### Image Script (iberpark_images.js)

#### getAccessToken()

Returns the Google Drive API access token needed to successfully make requests to the Google Drive API, also acts as an identifier for the client in the Google Drive API.

#### processAllImages(String accessToken)

Retrieves each image's ID from the client's Google Drive image directory. For each image ID, retrieves the image one by one (to save memory) and calls the uploadImageAndGetURI function to upload the image to Amazon S3. With the url address returned by the previous function, it then calls the processImageInArticle function to process the image.

#### uploadImageAndGetURI(String imageName, Uint8Array imageData)

Uploads the image data to Amazon S3 and returns the URL address of the uploaded image.

#### processImageInArticle(String imageName, String imageUrl)

Processes the image by parsing its name to the article SKU it belongs to, and adds that image to a map keyed to article SKUs.

#### sendImagesToSagal(String articleSku, Array images)

Sends the image data to sagal as an article property containing all the image urls for that article.
